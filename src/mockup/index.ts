import { SudokuValue } from "@/type"

export const INITIAL_VALUE: Array<Array<SudokuValue>> = [
  [ {}, {}, {}, {}, {}, {}, {}, {}, {} ],
  [ {}, {}, {}, {}, {}, {}, {}, {}, {} ],
  [ {}, {}, {}, {}, {}, {}, {}, {}, {} ],
  [ {}, {}, {}, {}, {}, {}, {}, {}, {} ],
  [ {}, {}, {}, {}, {}, {}, {}, {}, {} ],
  [ {}, {}, {}, {}, {}, {}, {}, {}, {} ],
  [ {}, {}, {}, {}, {}, {}, {}, {}, {} ],
  [ {}, {}, {}, {}, {}, {}, {}, {}, {} ],
  [ {}, {}, {}, {}, {}, {}, {}, {}, {} ],
];

export const TEST_VALUE: Array<Array<SudokuValue>> = [
  [ {value: [1]}, {value: [3], isDefault: true}, {value: [2], isDefault: true}, {value: [1]}, {value: [1]}, {value: [6], isDefault: true}, {value: [5], isDefault: true}, {value: [8], isDefault: true}, {value: [1]} ],
  [ {value: [4], isDefault: true}, {value: [1]}, {value: []}, {value: [5], isDefault: true}, {value: [1], isDefault: true}, {value: [8], isDefault: true}, {value: [2], isDefault: true}, {value: [1]}, {value: [7], isDefault: true} ],
  [ {value: [1]}, {value: [7], isDefault: true}, {value: [1]}, {value: [1]}, {value: [2], isDefault: true}, {value: [1]}, {value: [1]}, {value: [6], isDefault: true}, {value: [1]} ],
  [ {value: [1]}, {value: [1], isDefault: true}, {value: [1]}, {value: [1]}, {value: [5], isDefault: true}, {value: [1]}, {value: [1]}, {value: [9], isDefault: true}, {value: [8], isDefault: true} ],
  [ {value: [1]}, {value: [1]}, {value: [7], isDefault: true}, {value: [1]}, {value: [1]}, {value: [9], isDefault: true}, {value: [1]}, {value: [2], isDefault: true}, {value: [5], isDefault: true} ],
  [ {value: [9], isDefault: true}, {value: [2], isDefault: true}, {value: [1]}, {value: [1]}, {value: [8], isDefault: true}, {value: [1]}, {value: [1]}, {value: [1], isDefault: true}, {value: [1]} ],
  [ {value: [1]}, {value: [4], isDefault: true}, {value: [1]}, {value: [1]}, {value: [1]}, {value: [1]}, {value: [1]}, {value: [7], isDefault: true}, {value: [1]} ],
  [ {value: [7], isDefault: true}, {value: [1]}, {value: [1], isDefault: true}, {value: [8], isDefault: true}, {value: [4], isDefault: true}, {value: [2], isDefault: true}, {value: [1]}, {value: [1]}, {value: [6], isDefault: true} ],
  [ {value: [1]}, {value: [5], isDefault: true}, {value: [3], isDefault: true}, {value: [7], isDefault: true}, {value: [6], isDefault: true}, {value: [1]}, {value: [9], isDefault: true}, {value: [4], isDefault: true}, {value: [1]} ],
];

export const TEST_VALUE_2: Array<Array<SudokuValue>> = [
  [ {value: [1]}, {value: [1]}, {value: [3], isDefault: true}, {value: [1]}, {value: [6], isDefault: true}, {value: [1]}, {value: [1]}, {value: [8], isDefault: true}, {value: [9], isDefault: true} ],
  [ {value: [4], isDefault: true}, {value: [1]}, {value: [1]}, {value: [1]}, {value: [9], isDefault: true}, {value: [1]}, {value: [1]}, {value: [1]}, {value: [1]} ],
  [ {value: [6], isDefault: true}, {value: [1]}, {value: [9], isDefault: true}, {value: [1]}, {value: [3], isDefault: true}, {value: [1]}, {value: [1]}, {value: [2], isDefault: true}, {value: [1]} ],
  [ {value: [1]}, {value: [3], isDefault: true}, {value: [1]}, {value: [1]}, {value: [5], isDefault: true}, {value: [1]}, {value: [1]}, {value: [1]}, {value: [1]} ],
  [ {value: [1]}, {value: [9], isDefault: true}, {value: [1]}, {value: [2], isDefault: true}, {value: [1]}, {value: [3], isDefault: true}, {value: [4], isDefault: true}, {value: [6], isDefault: true}, {value: [8], isDefault: true} ],
  [ {value: [1]}, {value: [1]}, {value: [1]}, {value: [1]}, {value: [1]}, {value: [1]}, {value: [1]}, {value: [5], isDefault: true}, {value: [1]} ],
  [ {value: [1]}, {value: [1], isDefault: true}, {value: [1]}, {value: [1]}, {value: [8], isDefault: true}, {value: [1]}, {value: [9], isDefault: true}, {value: [1]}, {value: [6], isDefault: true} ],
  [ {value: [1]}, {value: [1]}, {value: [1]}, {value: [1]}, {value: [1], isDefault: true}, {value: [1]}, {value: [1]}, {value: [1]}, {value: [2], isDefault: true} ],
  [ {value: [9], isDefault: true}, {value: [4], isDefault: true}, {value: [1]}, {value: [1]}, {value: [2], isDefault: true}, {value: [1]}, {value: [8], isDefault: true}, {value: [1]}, {value: [1]} ],
];

export const TEST_VALUE_3: Array<Array<SudokuValue>> = [
  [ {value: [1]}, {value: [2]}, {value: [3], isDefault: true}, {value: [4]}, {value: [5]}, {value: [6]}, {value: [7], isDefault: true}, {value: [8]}, {value: [9]} ],
  [ {value: [4], isDefault: true}, {value: [5]}, {value: [6]}, {value: [7]}, {value: [9]}, {value: [8]}, {value: [1]}, {value: [2]}, {value: [3]} ],
  [ {value: [7], isDefault: true}, {value: [8], isDefault: true}, {value: [9]}, {value: [2], isDefault: true}, {value: [1]}, {value: [3]}, {value: [4], isDefault: true}, {value: [5], isDefault: true}, {value: [6]} ],
  [ {value: [2], isDefault: true}, {value: [6]}, {value: [4]}, {value: [9], isDefault: true}, {value: [3], isDefault: true}, {value: [7]}, {value: [5]}, {value: [8]}, {value: [1]} ],
  [ {value: [9], isDefault: true}, {value: [1]}, {value: [8]}, {value: [6]}, {value: [2], isDefault: true}, {value: [5], isDefault: true}, {value: [3], isDefault: true}, {value: [4], isDefault: true}, {value: [7], isDefault: true} ],
  [ {value: [5]}, {value: [3]}, {value: [7]}, {value: [1]}, {value: [8], isDefault: true}, {value: [4], isDefault: true}, {value: [2]}, {value: [6]}, {value: [9], isDefault: true} ],
  [ {value: [6]}, {value: [4], isDefault: true}, {value: [5], isDefault: true}, {value: [3]}, {value: [7], isDefault: true}, {value: [9], isDefault: true}, {value: [8]}, {value: [1], isDefault: true}, {value: [2], isDefault: true} ],
  [ {value: [3]}, {value: [9]}, {value: [2]}, {value: [8]}, {value: [4], isDefault: true}, {value: [1]}, {value: [6]}, {value: [7]}, {value: [5], isDefault: true} ],
  [ {value: [8]}, {value: [7]}, {value: [1], isDefault: true}, {value: [5]}, {value: [6]}, {value: [2]}, {value: [9], isDefault: true}, {value: [3]}, {value: [4]} ],
];
