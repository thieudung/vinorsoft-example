import React, { ChangeEvent, useRef, useState } from 'react'
import { ButtonComponent } from '../elements';
import classNames from 'classnames';
import { isPalindromeString } from '@/libs';

export const StringDemo = () => {
  const [stringInput, setStringInput] = useState<string>('');
  const [isExcludeSpace, setIsExcludeSpace] = useState<boolean>(true);
  const [validate, setValidate] = useState<boolean>(true);
  const [results, setResult] = useState<string[]>();
  const refText = useRef<HTMLTextAreaElement>(null);
  

  const handleOnChange = (e: ChangeEvent<HTMLTextAreaElement>) => {
    setValidate(true);
    setResult(undefined);
    setStringInput(e.target.value);
  }
  
  const handleViewResults = () => {
    const trimString = isExcludeSpace ? stringInput.trim().replace(/\s+/g, '') : stringInput.trim();

    if(trimString.length < 2) {
      setValidate(false);
    } else {
      let tmpString = trimString;
      let tmpRes: string[] = [];  

      for (let i = 0; i < tmpString.length; i++) {
        const strChecking = trimString.slice(i, tmpString.length);
        if(strChecking.length < 2) {
          break;
        }
        for (let j = tmpString.length; j > i + 1; j--) {
          const subString = tmpString.slice(i, j);
          const isPalindrome = isPalindromeString(subString);
          if(isPalindrome) {
            i = --j;
            tmpRes.push(subString);
          }
        }
      }

      setResult(tmpRes);
    }
  }

  const handleOnChangeCheckBox = () => {
    setIsExcludeSpace(!isExcludeSpace);
  }

  const handleReset = () => {
    setValidate(true);
    setStringInput('');
    setResult(undefined);
    refText.current?.focus();
  }

  return (
    <section className='flex flex-col space-y-6'>
      <div className='flex flex-col space-y-3 items-start relative'>
        <strong className='text-slate-600'>Input String: </strong>
        <div className='flex flex-row space-x-3'>
          <input
            type="checkbox" id="exclude-space" name="exclude-space"
            checked={isExcludeSpace}
            onChange={handleOnChangeCheckBox}
            disabled
          />
          <label htmlFor="exclude-space">{`Exclude white space!`}</label><br></br>
        </div>
        <textarea
          id="w3review"
          name="w3review"
          ref={refText}
          rows={4}
          cols={50}
          minLength={3}
          maxLength={5000}
          className={classNames(
            'w-full bg-slate-100 border-2 border-primary rounded-lg p-3',
            'hover:border-primary focus:border-primary !focus-visible:border-primary focus:outline-none',
            {"mb-24": !validate}
          )}
          value={stringInput}
          placeholder='Input string'
          onChange={handleOnChange}
        />
        {!validate && (
          <span className='text-xs text-highlight absolute -bottom-6 right-0'>(*) Dữ liệu đầu vào không hợp lệ</span>
        )}
      </div>

      <div className='flex space-x-3'>
        <ButtonComponent label='View Result' callback={handleViewResults} />
        <ButtonComponent label='Reset' callback={handleReset} />
      </div>

      {results !== undefined && (
        <div className='flex flex-col space-y-6 bg-primary text-white rounded-lg p-3'>
          <h5>Result</h5>
          <div>
            <div className='flex space-x-3'>
              <strong>Input:</strong>
              <div>{stringInput}</div>
            </div>

            <div className='flex space-x-3'>
              <strong>Output:</strong>
              <div className='text-secondary'>{!!results.length ? `( ${results.length} ) >>> ${results.join("; ")}` : "No data"}</div>
            </div>
          </div>
        </div>
      )}
    </section>
  )
}
