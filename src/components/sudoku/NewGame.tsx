import React, { Fragment, useEffect, useState } from 'react'
import { InputItemValue } from '../elements';
import { PositionSudokuCell, SudokuValue } from '@/type';

type Props = {
  sudoku: Array<Array<SudokuValue>>;
  isViewResult: boolean
}

export const NewGame = ({sudoku, isViewResult}: Props) => {
  const [game, setGame] = useState<Array<Array<SudokuValue>>>(JSON.parse(JSON.stringify(sudoku)));

  const handleUpdateSudokuCell = (value: number, position: PositionSudokuCell) => {
    
    const tmp = [...game]
    
    tmp[position.x][position.y].value?.push(value);

    setGame(tmp)
  }
  
  useEffect(() => {
    setGame(JSON.parse(JSON.stringify(sudoku)));
  },[sudoku])

  return (
    <div className='max-w-[480px] grid grid-cols-9 mt-12 border-primary border-2'>
      {[...Array(9)].map((_,x:number) => 
        <Fragment key={x}>
          {[...Array(9)].map((_,y:number) => {
            return (
              <InputItemValue
                key={`${x}-${y}`}
                isViewResult={isViewResult}
                sudokuCell={sudoku[x][y]}
                position={{x:x+1, y:y+1}}
                callback={(value: number)=>handleUpdateSudokuCell(value, {x:x, y:y})}
              />
            )
          })}
        </Fragment>
      )}
    </div>
  )
}
