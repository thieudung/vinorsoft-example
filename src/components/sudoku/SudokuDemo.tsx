import React, { useRef, useState } from 'react'
import { ButtonComponent } from '../elements'
import { NewGame } from '.';
import { SudokuValue } from '@/type';
import { INITIAL_VALUE, TEST_VALUE, TEST_VALUE_2, TEST_VALUE_3 } from '@/mockup';

export const SudokuDemo = () => {
  const [data, setData] = useState<Array<Array<SudokuValue>>>(INITIAL_VALUE);
  const [isViewResult, setIsViewResult] = useState<boolean>(false);

  const refTest = useRef<number>(0);

  const handleNewGame = () => {
    if(refTest.current % 2 !== 0) {
      setData(TEST_VALUE);
    } else {
      setData(TEST_VALUE_3);
    }
    refTest.current = refTest.current + 1;
  }

  const handleViewResult = () => {
    setIsViewResult(!isViewResult);
  }

  const handleSubmit = () => {
    
  }

  return (
    <section className='min-h-[720px]'>
      <div className='flex space-x-3'>
        <ButtonComponent label="New Game" callback={handleNewGame} />
        <ButtonComponent label={isViewResult ? "Hidden Result" : "View Result"} callback={handleViewResult} />
        <ButtonComponent label="Submit" callback={handleSubmit} />
      </div>

      <div className='w-fit m-auto'>
        <NewGame sudoku={data} isViewResult={isViewResult} />
      </div>
      
    </section>
    
  )
}

