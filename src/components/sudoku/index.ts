import { SudokuDemo } from './SudokuDemo'
import { NewGame } from './NewGame'

export {
  SudokuDemo,
  NewGame
}