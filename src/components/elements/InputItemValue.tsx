'use client'
import { PositionSudokuCell, SudokuValue } from '@/type';
import classNames from 'classnames';
import React, { ChangeEvent, useEffect, useId, useRef, useState } from 'react'

type Props = {
  sudokuCell: SudokuValue;
  isViewResult: boolean;
  position: PositionSudokuCell;
  callback: (value: number) => void;
}

export const InputItemValue = ({sudokuCell, position, isViewResult, callback}: Props) => {
  const refInput = useRef<HTMLInputElement>(null);
  const itemId = useId();
  const [currentValue, setCurrentValue] = useState<number | undefined>()

  const handleOnchange = (e: ChangeEvent<HTMLInputElement>) => {
    if(e.target.value.length > 1 || !e.target.value) {
      return;
    } else {
      setCurrentValue(Number(e.target.value));
      callback(Number(e.target.value));
      refInput.current?.blur();
    }
    return;
  }

  const handleOnFocus = () => {
    if(!!refInput.current?.value) {
      refInput.current.select();
    }
    return;
  }

  useEffect(()=> {
    if(sudokuCell.isDefault) {
      setCurrentValue(sudokuCell?.value?.[0]);
    }
  },[sudokuCell])

  useEffect(()=> {
    if(isViewResult) {
      setCurrentValue(sudokuCell?.value?.[0]);
    } else {
      setCurrentValue(sudokuCell.isDefault ? sudokuCell?.value?.[0] : sudokuCell?.value?.[1]);
    }
  },[isViewResult])

  return (
    <input
      type="number"
      id={itemId}
      name={`${position.x}-${position.y}`}
      maxLength={1}
      className={classNames(
        'border-[1px] rounded-none aspect-square text-center',
        'font-normal text-xl md:text-2xl',
        'focus:bg-[#dcdcdc] focus:outline-none',
        {'border-b-2 border-b-primary': position.x % 3 === 0 && position.x !==9},
        {'border-r-2 border-r-primary': position.y % 3 === 0 && position.y !==9},
        {'bg-secondary': sudokuCell.isDefault},
      )}
      //value={currentValue}
      defaultValue={currentValue}
      disabled={sudokuCell.isDefault || isViewResult}
      ref={refInput}
      onChange={handleOnchange}
      onFocus={handleOnFocus}
    />
  )
}
