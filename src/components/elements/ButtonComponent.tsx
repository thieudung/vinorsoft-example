import React from 'react'

type Props = {
  label: string;
  callback: () => void;
}

export const ButtonComponent = ({label, callback}: Props) => {
  return (
    <button
      className="w-fit bg-secondary px-6 py-2 rounded-full hover:bg-primary hover:text-white"
      onClick={callback}
    >
      {label}
    </button>
  )
}
