'use client';

import { StringDemo } from "@/components/cut-string";
import { ButtonComponent } from "@/components/elements";
import { SudokuDemo } from "@/components/sudoku";
import classNames from "classnames";
import { Fragment, useEffect, useState } from "react";

type Example = {
  id: number,
  title: string,
  content: string[]
}

const examples: Example[] = [
  {
    id: 1,
    title: 'Xây dựng trò chơi sudoku.',
    content: [
      '1 button để sinh đề ngẫu nhiên',
      '1 button giải đề',
      'Người dùng được điền số vào từng ô (ô sẽ tô đỏ khi người dùng điền sai)',
      'Sử dụng html, css, js (ts), reactjs'
    ]
  },
  {
    id: 2,
    title: 'Cho 1 xâu ký tự có độ dài <= 5000 ký tự, tìm cách cắt ít nhất để thành toàn các xâu đối xứng và in ra kết quả.',
    content: [
      'Đầu vào: aabbaabb',
      'Đầu ra: aabbaa và bb'
    ]
  }
]

export default function Home() {
  const [currentExample, setCurrentExample] = useState<number>(1);
  const [isDemo, setIsDemo] = useState<boolean>(false);

  
  useEffect(()=>{
    setIsDemo(false);
  },[currentExample])

  return (
    <main className="max-w-[calc(100%_-_48px)] md:max-w-[1200px] mx-auto py-12 flex flex-col md:flex-row space-x-0 md:space-x-12 space-y-12 md:space-y-0 select-none">
      <div className="max-w-sm w-full border-[1px] border-gray-200 p-6 rounded-2xl">
        <h6 className="pb-6">(FE) Thiều Phan Quang Dũng</h6>
        <h5>Bài kiểm tra đánh giá năng lực</h5>
        
        <div className="flex flex-col space-y-6 mt-12">
          {examples.map(item => {
            return (
              <div key={item.id} className="flex flex-col space-y-3">
                <div
                  className={classNames(
                    "font-normal",
                    {"cursor-pointer": currentExample!==item.id},
                    {"hover:text-primary": currentExample!==item.id}
                  )}
                  onClick={()=>setCurrentExample(item.id)}
                >
                  {`Câu ${item.id}: ${item.title}`}
                </div>
                {item.id === currentExample && (
                  <div className="flex flex-col space-y-2">
                    <p>Yêu cầu:</p>
                    <ul className="ml-6">
                      {item.content.map((item, index:number) => 
                        <li key={index}><span className="pr-3">-</span>{item}</li>
                      )}
                    </ul>
                    <div className="flex justify-end">
                      <ButtonComponent label="Demo" callback={()=>setIsDemo(true)} />
                    </div>
                    
                  </div>
                )}
                
              </div>
            )
          })}
        </div>
      </div>

      {isDemo && (
        <div className="w-full border-[1px] border-gray-200 p-6 rounded-2xl">
          {currentExample === 1 && (
            <SudokuDemo />
          )}
          
          {currentExample === 2 && (
            <StringDemo />
          )}
        </div>
      )}
    </main>
  );
}
