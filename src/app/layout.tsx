import type { Metadata } from "next";
//import { Inter } from "next/font/google";
import { Unbounded } from "next/font/google";
import "./globals.css";

const unbounded = Unbounded({
  subsets: ["latin"],
  variable: '--font-Unbounded' 
});

export const metadata: Metadata = {
  title: "Thiều Phan Quang Dũng",
  description: "Vinorsoft - Bài kiểm tra đánh giá năng lực",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body className={unbounded.className}>
        {children}
      </body>
    </html>
  );
}
