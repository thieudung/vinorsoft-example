export const isPalindromeString = (str: string): boolean => {
  if(str.length < 2) {
    return false;
  } 
  const reverseString = str.split('').reverse().join('');
  return str === reverseString;
}

export const createNumber = (): number => {
  return Math.floor(Math.random() * 9) + 1;
}

export const X = () => {
  const tmp: number[] = [];
  [...Array(3)].map(() => {
    do {
      const val = createNumber();  
      if(!tmp.includes(val)) {
        tmp.push(val)
      }
    }
    while (tmp.length === 3);
  })
  return tmp;
};

export const Y = () => {
  const tmp: number[] = [];
  [...Array(3)].map(() => {
    do {
      const val = createNumber();  
      if(!tmp.includes(val)) {
        tmp.push(val)
      }
    }
    while (tmp.length === 3);
  })
  return tmp;
};