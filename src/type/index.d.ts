export type SudokuValue = {
  value?: number[] | null | undefined;
  isDefault?: boolean;
}

export type PositionSudokuCell = {
  x: number;
  y:number;
}